import sys
import os
import pylab
import numpy as N

from tailz.syn import Syn

""" Construct a synthetic dataset from a single Gaussian distribution """

modelpath = 'data/single_gaussian.pickle'
traincat = "data/single_gaussian_train.npy"
testcat = "data/single_gaussian_test.npy"

GG = Syn(logz=False, cachefile=modelpath)

# The data consists of redshift and 2 colors, so dimension is 3.
# Here is the mean and variance of the distribution.  The first dimension
# is redshift.  The covariances are diagonal.
# weight w=1 because there is only one component.
dim = 3
mu = (0.5, 0, 0)
cov = (.005, 1, 1)
w = 1

# Construct the Gaussian mixture model with these parameters
GG.set([(mu,cov,w)])

# save the definition
GG.save()

# sample with 1000 points and save the data
# seed is set so output will always be the same...
data = GG.sample(n=1000, random_state=1, save=traincat)


# define the test data
test = N.zeros((1,dim-1))
if not os.path.exists(testcat):
    N.save(testcat, test)

print "> done"
