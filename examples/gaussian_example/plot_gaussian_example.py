import sys
import os
import pylab
import numpy as N
import cPickle as pickle

from tailz.syn import Syn

""" Construct a synthetic dataset from a single Gaussian distribution """

modelpath = 'data/single_gaussian.pickle'
traincat = "data/single_gaussian_train.npy"
testcat = "data/single_gaussian_test.npy"
pdfcat = "results/pdf_gauss.pickle"

GG = Syn(logz=False, cachefile=modelpath)

test = N.load(testcat)

z,p_estimate = pickle.load(file(pdfcat))

pdfs = GG.pdf(test,z)

pylab.semilogy(z,pdfs[0],lw=2,c='k')
pylab.fill_between(z,p_estimate[0],1e-10,facecolor='hotpink')

pylab.ylim(.001,0.1)

pylab.savefig("results/plot.png")

pylab.show()
