import sys,os
import numpy as N

from tailz.syn import Syn

ncat = 5e4
ntest = 10

catalog = "data/vipers_pdr1.npy"
fitfile = "data/syn_gmm.npy"
mockcat = "data/zcat.npy"
testcat = "data/testcat.npy"



S = Syn(cachefile=fitfile)

if os.path.exists(fitfile): 
    print "> fit already done! will not overwrite output %s"%fitfile
else:
    print "> loading catalog", catalog
    data = N.load(catalog)
    S.fit(data, k=8)
    S.save()

# Now we can construct mock samples
S.sample(n = ncat, save=mockcat)
print "> wrote %s"%mockcat

# and a test sample
testsamp = S.sample(n = ntest)
testsamp = testsamp[:,1:]
N.save(testcat, testsamp)

print "> wrote %s"%testcat

print "> done"
