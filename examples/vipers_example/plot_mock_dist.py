import sys
import matplotlib
matplotlib.rc_context(rc={'backend': 'PDF'})
import pylab
import numpy as N
import cPickle as pickle
import time
from matplotlib.ticker import MaxNLocator,MultipleLocator,AutoMinorLocator

from tailz.syn import Syn
from tailz.utils import lowerwater


path = "data/syn_gmm.npy"
labels = ("Redshift","u-g","g-r","r-i","i-z")


n = 100000
S = Syn(cachefile=path)
data = S.sample(n = n)

nbins = 50

#data = data[:10000,:]


pylab.figure(figsize=(4,4))

print data.shape
n,dim = data.shape


low,high = N.percentile(data,(5,95),axis=0)
h = high-low
low -= h/5
high += h/5
print low
print high

for i in range(dim):
    binsi = N.linspace(low[i],high[i],nbins)
    for j in range(dim):

        if i<j: continue
        pylab.subplot(dim,dim,i*dim+j+1)

        binsj = N.linspace(low[j],high[j],nbins)

        if i==j:
            h,e = N.histogram(data[:,i],bins=binsi, density=True)
            c = (e[1:]+e[:-1])/2.
            pylab.plot(c,h,c='navy')
            pylab.title(labels[i])
            pylab.xlim(low[i],high[i])
            continue

        binsj = N.linspace(low[j],high[j],nbins)


        ext = [binsj[0],binsj[-1],binsi[0],binsi[-1]]

        h,ey,ex = N.histogram2d(data[:,i],data[:,j],bins=(binsi,binsj))

        plevels = [0.9,0.75,0.5,0.25,0.1,0]
        lev = lowerwater(h,plevels,N.sum(h))


        #pylab.imshow(h,extent=ext,aspect='auto',cmap='viridis_r',origin='lower')
        pylab.contourf(h,levels=lev,extent=ext,cmap='viridis_r')

        pylab.xlim(low[j],high[j])
        pylab.ylim(low[i],high[i])
   
for i in range(5):
    for j in range(5):
        if i<j: continue


        ax = pylab.subplot(5,5,i*5+j+1)

        ax.xaxis.set_major_locator(MaxNLocator(3))
        ax.xaxis.set_minor_locator(AutoMinorLocator(5))
        ax.yaxis.set_major_locator(MaxNLocator(3))
        ax.yaxis.set_minor_locator(AutoMinorLocator(5))

        ax.set_xlim(low[j],high[j])
        if i>j:
            ax.set_ylim(low[i],high[i])

        if j>0:
            ax.yaxis.set_ticklabels([])  
                
        if i<4:
            ax.xaxis.set_ticklabels([])

        if j == 0:
            if i>0: ax.set_ylabel(labels[i])
            if i==0: ax.yaxis.set_ticklabels([])  
            #if i==0: ax.set_ylabel("PDF")
        if i==4:  
            ax.set_xlabel(labels[j])


pylab.subplots_adjust(hspace=0,wspace=0)

pylab.savefig("ccplot.png")
#pylab.show()
