try:
    from setuptools import setup, Extension
except ImportError:
    from distutils.core import setup, Extension

setup(
    name = 'TailZ',
    version = '0.1.dev0',
    description = '',
    author = 'Ben Granett',
    author_email = 'ben.granett@brera.inaf.it',
    url = 'none',
    download_url = 'none',
    packages = ['tailz',],
    license = 'Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description = open('README.txt').read(),
    install_requires = [
        'numpy',
        'scipy',
        'scikit-learn',
	],
    test_suite = 'nose.collector',
    tests_require = ['nose'],
    entry_points = {        
        'console_scripts': ['tailz = tailz.main:main']
    }
)
