TailZ
================
*Probing the sparse tails of redshift distributions*

* Check it out from the `code repository <https://bitbucket.org/bengranett/tailz/src>`_
* Find documentation and examples on the `wiki <https://bitbucket.org/bengranett/tailz/wiki>`_

Tailz is a code for estimating the redshift distributions of photometric samples of galaxies.  It uses a Voronoi tessellation density estimator to interpolate the distribution and performs well even in the tails.

Features
---------
* compute redshift PDFs of individual galaxies

Examples
----------
Full examples may be found on the `TailZ wiki <https://bitbucket.org/bengranett/tailz/wiki>`_.

In brief, ``Tailz`` may be run from the command line::

  runtailz -c tailz.config --zcat=trainingcat.txt --outdir=results

Options may be placed in a configuration file or specified on the command line.  Online documentation is provided::

  runtailz --help 

``Tailz`` is also a python module::

  import tailz
  T = tailz.tailz(training_cat, **params)
  pdf = T.predict(photo_cat)


Install
----------
Install in a single user environment::

  cd tailz
  python setup.py install --user


``TailZ`` is developed for python2.7.  The following python packages must also be installed.

* `numpy <http://numpy.org>`_
* `scipy <http://scipy.org>`_
* `scikit-learn <http://scikit-learn.org>`_

What to cite
----------
If you use the software please cite one of the following.

* `Granett B.R., Astronomy and Computing, Submitted, 2016 <http://adsabs.harvard.edu/abs/2016arXiv160203121G>`_
* `ascl:1602.013 <http://ascl.net/1602.013>`_

License
----------
The software is made available under the MIT License (MIT).  Refer to ``LICENSE.txt``.