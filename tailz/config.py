import sys,os
import time
import ConfigParser
import re

def gitcommit():
    """ Return the commit hash """
    try:
        import git
    except ImportError:
        return ""
    repo = git.Repo(".",search_parent_directories=True)
    return repo.head.commit.hexsha


def write_config(name,fid=sys.stdout,params={}):
    """ Write configuration file from parameter dictionary.

    Parameters
    ----------
    name : str
        Section name to look at in the config file.
    fid : file object
        Destination to write to, default is STDOUT
    params : dict
        Dictionary with key value pairs to write

    Output
    ------
    None
     """
    C = ConfigParser.RawConfigParser(allow_no_value=True)
    C.add_section(name)
    for key,val in params.items():
        C.set(name, key, val)

    fid.write("# parameter set\n")
    fid.write("# %s"%gitcommit())
    currenttime = time.asctime()
    fid.write("# %s\n"%currenttime)
    C.write(fid)



def read_config(name,path,params={}):
    """ Read configuration file from disk

    Parameters
    ----------
    name : str
        Section name to look at in the config file.
    path : str
        Path to file on disk
    params : dict
        Dictionary with defaults used to deterine type conversions.  This 
        dictionary will be updated with new values

    Output
    ------
    dict :
      Parameter values are stored in the params dictionary and returned.
    """
    if not os.path.exists(path):
        print "Config file does not exist: %s. Stopping."%path
        exit(-1)

    C = ConfigParser.RawConfigParser(allow_no_value=True)

    try:
        C.read(path)
    except ConfigParser.Error as e:
        print "Error reading the config file %s. Stopping."%path
        print e
        exit(-1)

    for name in C.sections():
        for k in params.keys():
            if not C.has_option(name,k): continue

            t = type(params[k])

            if t==type(1):
                v = C.getint(name,k)
            elif t==type(1.0):
                v = C.getfloat(name,k)
            elif t==type(True):
                v = C.getboolean(name,k)
            elif t==type([1,2]):
                s = C.get(name,k)
                # remove brackets
                s = re.sub("\[|\]|\(|\)","",s)
                s = re.sub(","," ",s) # replace separator by white space
                # values will be read as floats
                v = [float(x) for x in s.split()]
            else:
                v = C.get(name,k)
            params[k] = v
    return params
    
