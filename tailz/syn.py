import sys,os
import numpy as N

import cPickle as pickle

from sklearn import mixture

class SimpleGMM:
    def __init__(self, components=[]):
        """ """

        self.means_ = []
        self.covars_ = []
        self.weights_ = []

        for a,b,c in components:

            b = N.array(b)
            if b.size==len(b):
                b = N.diag(b)

            self.means_.append(a)
            self.covars_.append(b)
            self.weights_.append(c)

        self.dim = len(self.means_[0])

    def add(self, mu=0, cov=1, w=1):
        """ """
        if self.dim is not None:
            assert(len(mu)==self.dim)
        else:
            self.dim = len(mu)

        cov = N.array(cov)
        if cov.size==len(cov):
            cov = N.diag(cov)

        self.means_.append(mu)
        self.covars_.append(cov)
        self.weights_.append(w)

    def sample(self, n, random_state=None):
        """ """
        N.random.seed(random_state)

        norm = N.sum(self.weights_)
        if norm==0:
            print "weights are all 0!"
            return
        w = N.array(self.weights_)*1./norm

        ncomp = len(w)

        ii = N.random.choice(N.arange(ncomp), n, w)

        m = N.bincount(ii)

        samp = []
        for i in range(ncomp):
            s = N.random.multivariate_normal(self.means_[i],self.covars_[i],m[i])
            samp.append(s)

        samp = N.vstack(samp)

        order = N.random.uniform(0,1,len(samp)).argsort()
        samp = samp[order]

        return samp


class Syn:

    def __init__(self, logz = True, cachefile=None):
        """ """
        self.cachefile = cachefile

        self.logz = logz
        self.load(self.cachefile)

    def load(self, path):
        """ """
        if os.path.exists(path):
            self.params = pickle.load(file(path))
            self.dim = len(self.params[0])

    def save(self, path=None):
        """ """
        if path is None: path = self.cachefile

        dir = os.path.dirname(path)
        if not os.path.exists(dir): os.mkdir(dir)

        if os.path.exists(path):
            print "File exists: %s.  Will not overwrite."%path
        else:
            pickle.dump(self.params, file(path,'w'))

    def set(self, comp):
        """ """
        print comp
        G = SimpleGMM(comp)
        dim = G.dim
        mu = N.zeros(dim)
        sig = N.ones(dim)
        self.dim = dim
        self.params = (mu,sig,G,-1)
        print "dim=",dim


    def fit(self, data, k=30, loops=10):
        """ Fit the data with a Gaussian mixture model.  
        Uses the sklearn.mixture.GMM routine.

        Parameters
        ----------
        data : ndarray
        k : int
            number of components
        loops : int
            try a number of times and take the solution that maximizes the Akaike
            criterion.

        Output
        ------
        None

        """
        print "running gausian fit with n components=%i"%k

        # log z
        if self.logz:
            z = data[:,0]
            assert(N.all(z>0))
            datat = data.copy()
            datat[:,0] = N.log(z)
        else:
            datat = data

        # whiten data
        mu = N.mean(datat, axis=0)
        sig = N.std(datat, axis=0)
        print mu,sig
        data_w = (datat - mu)/sig

        best = None
        best_aic = 1e10
        for i in range(loops):
            G = mixture.GMM(n_components=k, covariance_type='full')
            G.fit(data_w)
            if not G.converged_: continue
            aic = G.aic(data_w)
            print i, "AIC %f (best so far %f)"%(G.aic(data_w),best_aic)
            if aic < best_aic:
                best = G
                best_aic = aic

        if best == None:
            print "dead! no good gmm fit was found!"
            exit(-1)

        G = best

        self.dim = len(mu)
        self.params = (mu, sig, G, best_aic)


    def sample(self, n=1e5, random_state=None, save=None):
        """ """
        if save is not None:
            if os.path.exists(save):
                print "save file %s already exists.  Will not overwrite"%save

        mu, sig, G, best_aic = self.params

        print "generating data"
        syndata = G.sample(int(n),random_state=random_state)

        syndata = syndata*sig + mu

        if self.logz:
            syndata[:,0] = N.exp(syndata[:,0])

        if save is not None:
            N.save(save, syndata)

        print "done",syndata.shape
        return syndata
    

    def pdf(self, colors, z):
        """ """
        mu, sig, G, best_aic = self.params

        print mu,sig

        centers = G.means_
        weights = G.weights_
        cmats = G.covars_

        ngroups = len(centers)
        dim = len(cmats[0])

        cinv = []
        norm = []
        for C in cmats:
            print C
            cinv.append(N.linalg.inv(C))
            norm.append( N.sqrt(2*N.pi)**dim * N.sqrt(N.linalg.det(C)))

        if self.logz:
            zt = N.log(z)
        else:
            zt = z

        x = N.zeros((len(zt), dim))
        x[:,0] = zt

        pdfout = []
        for c in colors:
            x[:,1:] = c
            print x.shape,mu.shape,sig.shape
            xt = (x-mu)/sig

            p = N.zeros(len(zt),dtype='d')
            for i in range(ngroups):
                d = xt - centers[i]
                a = N.sum(d.T*N.dot(cinv[i],d.T), axis=0)
                p += weights[i]*1./norm[i]*N.exp(-0.5*a)
            p /= N.sum(p)
            pdfout.append(p)

        return pdfout



