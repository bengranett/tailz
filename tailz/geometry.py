import numpy as N
from scipy.spatial import Delaunay



def simplex_volume_general(v):
    """Cayley-Menger Determinant
    http://mathworld.wolfram.com/Cayley-MengerDeterminant.html
    """
    coeff = [-1,2,-16,288,-9216, 460800]
    n,dim = v.shape
    mat = 1 - N.eye(n+1)
    for i in range(n):
        for j in range(1,n):
            d = N.sum((v[i]-v[j])**2)
            mat[1+i,1+j] = d
            mat[1+j,1+i] = d
    det = N.linalg.det(mat)
    a = det*1./coeff[dim]
    if a<0:
        # print "error in simplex volume",dim,a
        # print "volume set to 0"
        volume = 0
    else:        
        volume = N.sqrt(a)
    return volume

def simplex_volume(v):
    """ """
    n,dim = v.shape
    if dim>3:
        return simplex_volume_general(v)

    if dim==0:
        volume = 0
    elif dim==1:
        volume = N.abs(v[0]-v[1])
    elif dim==2:
        a = v[0]-v[2]
        b = v[1]-v[2]
        volume = 0.5*N.abs(N.cross(a,b))
    elif dim==3:
        a=v[0]-v[3]
        b=v[1]-v[3]
        c=v[2]-v[3]
        volume = 1./6 * N.abs(N.dot(a,N.cross(b,c)))      
    else:
        print "bad dimension",dim
        exit(-1)
        
    return volume



def centroid(vert):
    """ Compute the centroid of a convex hull.  
    First breaks the volume into Delaunay triangles (simplices), then computes
    the weighted average of triangle centroids. """
    tri = Delaunay(vert)
    n,dim = vert.shape
    running_sum = N.zeros(dim)
    tot_volume = 0.
    #print "n sim",n,dim,len(tri.simplices)
    for s in tri.simplices:
        v = tri.points[s]
        volume = simplex_volume(v)
        centroid = N.mean(v,axis=0)
        running_sum += volume*centroid
        tot_volume += volume
    cent = running_sum/tot_volume
    return cent,tot_volume

def check_inside(tree,index,test):
    """ """
    d,ind = tree.query(test,k=1)
    ii = ind==index
    return ii



def sphere_surface_area(radius,dim):
    """Compute the surface area of a hypersphere

    Parameters
    ----------
    radius : float
        Radius of sphere
    dim : int
        Spatial dimension
    """
    if dim==1:
        sa = 1
    elif dim==2:
        sa = 2*N.pi*radius
    elif dim==3:
        sa = 4.*N.pi*radius**2
    elif dim==4:
        sa = 2*N.pi**2*radius**3
    elif dim==5:
        sa = 8./3*N.pi**2 * radius**4
    elif dim==6:
        sa = radius**5
    else:
        print "sphere surface area in high dimension not implemented!",dim
        exit(-1)
    return sa

def sphere_volume(radius,dim):
    """Compute volume of a hypersphere

    Parameters
    ----------
    radius : float
        Radius of sphere
    dim : int
        Spatial dimension
    """
    if dim==1:
        volume = 2*radius
    if dim==2:
        volume = N.pi * radius**2
    elif dim==3:
        volume = 4./3*N.pi * radius**3
    elif dim==4:
        volume = N.pi**2/2 * radius**4
    elif dim==5:
        volume = 8*N.pi**2/15 * radius**5
    elif dim==6:
        volume = 1./6 * N.pi**3 * radius**6
    else:
        print "sphere volume in high dimension not implemented!",dim
        exit(-1)
    return volume

def sample_ball(dim,radius,npoints=None,n=None):
    """ Draw points uniformly from interior of a hyperball. """
    if npoints==None:
        volume = sphere_volume(radius,dim)
        npoints = n*volume
    r = N.random.uniform(0,1,npoints)**(1./dim)
    u = N.random.normal(0,1,(dim,npoints))
    n = N.sqrt(N.sum(u**2,axis=0))
    u = u/n * r*radius
    return u.T

def sample_sphere(dim,radius,dr,npoints=None,n=None):
    """ Draw points uniformly from surface of a hypersphere. """
    if npoints==None:
        sa = sphere_surface_area(radius,dim)
        npoints = n*sa*dr

    u = N.random.normal(0,1,(dim,npoints))
    n = N.sqrt(N.sum(u**2,axis=0))
    u = u/n * radius
    return u.T


if __name__=="__main__":
    print "surface area of hyperspheres"
    r = 180./N.pi
    print "dimension\tarea (deg^d)"
    for d in range(2,6):
        print "%i\t%5.2e"%(d, sphere_surface_area(r,d))

