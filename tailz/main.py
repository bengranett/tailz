import sys
import os
import argparse

import cPickle as pickle

import config
from tailz import Tailz

def increment_path(p, maxit=10000):
    """ """
    if not os.path.exists(p): return p

    a,b = os.path.splitext(p)
    i = 0
    while i<maxit:
        pt = '%s.%i%s'%(a,i,b)
        if not os.path.exists(pt):
            return pt
        i += 1
    print "dead! could not construct a unique file name!"
    exit(-1)

def write_inputs(params):
    """ Write the parameters for this run to a file. """
    if not os.path.exists(params['outdir']):
        os.mkdir(params['outdir'])
    path = "%s/inputs_%s.config"%(params['outdir'],params['runtag'])
    path = increment_path(path)
    out = file(path,"w")
    config.write_config(Tailz.__name__,out,params=params)
    out.close()


def go(params):
    """ """
    pdf_path = "%s/pdf_%s.pickle"%(params['outdir'], params['runtag'])
    pdf_path = increment_path(pdf_path)

    # Initialize ...
    T = Tailz(**params)

    # ... and run
    if params['mode'] == 'plan':
        T.plan()
        T.save_cache()
    elif params['mode'] == 'predict':
        pdfs = T.predict()
        T.save_cache()
        pickle.dump(pdfs, file(pdf_path,'w'))
        print "Output: %s"%pdf_path

def main():
    """ params = {'zbounds': (0,2),
              'zstep_fine': 0.001,
              'zstep': 0.01,
              'tess_npoints': 100,
              'tess_rad_frac': 10,
              'tess_maxloops': 1000000,
              'runtag': '',
              'tmpdir': None,
              'verbose': False,
              'nproc': 1,
          }
    """
    params = Tailz.params

    parser = argparse.ArgumentParser(description="TailZ")

    paramgroup = parser.add_argument_group('configuration options')
    paramgroup.add_argument('-c','--config', metavar='file', required=False, help='input parameter file')
    paramgroup.add_argument('--writeconfig', action='store_true', help='write default parameter file to screen and exit')

    execgroup = parser.add_argument_group('execution options')
    execgroup.add_argument('-m','--mode', metavar='mode', choices=['train', 'predict'],
        help='if mode is \'plan\' execution stops after the tessellation is \
        constructed from the training catalog.  If mode is \'predict\' the \
        tessellation is constructed as needed and pdfs are estimated for the \
        photometric catalog. It is not necessary to run plan first.')

    catgroup = parser.add_argument_group('catalog options')
    catgroup.add_argument('--zcat', metavar='path',  type=str, help='Training set catalog containing redshift and photometric parameters')
    catgroup.add_argument('--zindex_z', metavar='i', nargs=1, type=int, help='Column number of redshift parameter in zcat file')
    catgroup.add_argument('--zindex_p', metavar='i', nargs='+', type=int, help='Column numbers of phot parameters in zcat file')

    catgroup.add_argument('--pcat', metavar='path',  type=str, help='Test catalog with photometric params')
    catgroup.add_argument('--pindex_p', metavar='i', nargs='+', type=int, help='Column numbers of phot parameters in pcat file')

    catgroup.add_argument('--runtag',metavar='tag',type=str,help='run tag to be appended to output files')
    catgroup.add_argument('--outdir',metavar='dir',type=str,help='directory to write outputs')
    catgroup.add_argument('--tmpdir',metavar='dir',type=str,help='directory to store temporary files')

    pdfgroup = parser.add_argument_group('pdf options')
    pdfgroup.add_argument('--zrange',metavar=('z1','z2'), nargs=2, type=float,help='Redshift range for pdf : zlow zhigh',)
    pdfgroup.add_argument('--zstep',metavar='dz',type=float,help='Redshift step for pdf')

    tessgroup = parser.add_argument_group('tessellation options')

    tessgroup.add_argument('--tess_rad_frac',metavar='x',type=float,help='Tessellation parameter step=0.5 R_nn/rad_frac')
    tessgroup.add_argument('--tess_npoints',metavar='n',type=int,help='Tessellation parameter number of sample points per cell')
    tessgroup.add_argument('--tess_zstep',metavar='dz',type=float,help='Redshift step for tessellation')
    tessgroup.add_argument('--tess_maxloops',metavar='n',type=float,help='Maximum iterations to do')

    parser.add_argument('--nproc', metavar='n', type=int, help='number of processors')
    parser.add_argument('--verbose', metavar='bool', type=bool, help='write more messages (True, False)')
    parser.add_argument('-v','--version', action='version', version='%(prog)s '+'%s'%(Tailz.__version__))

    parser.parse_args()

    args = parser.parse_args()
    argdict = vars(args)

    if argdict['writeconfig']:
        # write example parameter file and exit
        config.write_config(Tailz.__name__,params=Tailz.params)
        exit()

    if argdict['config'] is not None:
        # read parameter file
        params = config.read_config(Tailz.__name__, args.config, params)

    # apply command line options
    for k in argdict:
        if argdict[k] is None: continue
        if params.has_key(k):
            params[k] = argdict[k]

    # write file with effective configuration
    write_inputs(params)

    # go!
    go(params)

if __name__=="__main__":
    main()

