import sys,os
from collections import OrderedDict
import time

import numpy as N

import cPickle as pickle

import utils
import ioutils
import tessellation

import sklearn
import sklearn.neighbors
from multiprocessing import Pool


class Tailz:
    """ TailZ
    """
    __version__ = "0.1.dev0"

    def __init__(self, training_set=None, **parameters):
        """ 
        TailZ

        Parameters
        ----------
        training_set : array shape (n,m) , optional
            Training set catalog with n sources each with m features.  First 
            feature should be redshift.
        path  : str, optional
            If training_set is not given, provide a path to a catalog on disk
        tmpdir : str, optional
            Directory to save cached data
        verbose: bool
            Turn on verbose output
        
        """
        self.setup_done = False

        self.params = OrderedDict([
            ('mode','predict'),
            ('zcat',None),
            ('zindex_z',0),
            ('zindex_p',[1,2,3]),
            ('pcat',None),
            ('pindex_p',[0,1,2]),
            ('zrange',[0.0,2.0]),
            ('zstep', 0.01),
            ('error_samp_n', 10),
            ('tess_zstep', 0.001),
            ('tess_npoints', 100),
            ('tess_rad_frac', 10.0),
            ('tess_maxloops', 1000000),
            ('runtag', 'test'),
            ('outdir', 'results'),
            ('tmpdir', 'results'),
            ('cachepath_vol', '%s/cache_vol_%s.pickle'),
            ('cachepath_nn', '%s/cache_nn_%s.pickle'),
            ('verbose', False),
            ('nproc', 1),
            ('batch_size', 100),
        ])

        for key in parameters.keys():
            if self.params.has_key(key): self.params[key] = parameters[key]

        # set up cache directory
        if self.params['tmpdir'] is not None:
            if not os.path.exists(self.params['tmpdir']):
                os.mkdir(self.params['tmpdir'])

        # load training set from array
        if training_set is not None:
            self.training_set = N.array(training_set)

        # or from disk
        elif self.params['zcat'] is not None:
            self._message("reading %s"%self.params['zcat'])
            self.training_set = N.array(ioutils.load_file(self.params['zcat']))

        else:
            self.training_set = None

        if self.training_set is not None:
            self.n,self.dim = self.training_set.shape
            self._message("training set: N=%i dimension=%i"%(self.n,self.dim))

            if self.dim > self.n:
                self._message("warning: dimension is larger than number of objects.")
        
            # initialize voronoi sites
            self._init_sites()

    def __enter__(self):
        """ On start up attempt to load any cached data. """
        return self

    def __exit__(self, type, value, traceback):
        """ On exit dump intermediate results to cache. """
        self.save_cache()

    def _message(self, m):
        """ Print a message to the screen. """
        if self.params['verbose']:
            print "> %s: %s"%(self.__class__.__name__, m)

    def _init_sites(self):
        """ Initialize and Standardize the training set """
        if self.params['tmpdir'] is not None:
            if not os.path.exists(self.params['tmpdir']):
                os.mkdir(self.params['tmpdir'])
            self.params['cachepath_vol'] %= self.params['tmpdir'], self.params['runtag']
            self.params['cachepath_nn'] %= self.params['tmpdir'], self.params['runtag']
        else:
            self.params['cachepath_vol'] = None
            self.params['cachepath_nn'] = None

        mean = N.mean(self.training_set,axis=0)
        sigma = N.std(self.training_set,axis=0)

        self.data_norm = (mean,sigma)
        self.ztransform = (mean[0],sigma[0])

        self.sites = self._normalize(self.training_set)

        self.data_norm = (mean,sigma)

        center = N.mean(self.sites,axis=0)
        r2 = N.max(N.sum((self.sites-center)**2,axis=1))
        self.boundary = (center,r2)

        self.tree = sklearn.neighbors.KDTree(self.sites)

        self.vol_cache = {}
        self.neighbors_cache = {}
        self.caches = self.vol_cache, self.neighbors_cache

        self.load_cache()


    def _normalize(self, x, errors=False):
        """ """
        mean,sigma = self.data_norm

        if errors:
            mean = N.zeros(len(mean))

        if self.dim == (x.shape[1]+1):
            y = (x-mean[1:])/sigma[1:]
        else:
            y = (x-mean)/sigma

        return y

    def load_cache(self):
        """ """
        if self.params['cachepath_vol'] is None: return
        if os.path.exists(self.params['cachepath_vol']):
            self.vol_cache = pickle.load(file(self.params['cachepath_vol']))
            self.neighbors_cache = pickle.load(file(self.params['cachepath_nn']))
            self.caches = self.vol_cache, self.neighbors_cache
            self._message("read in volume cache %s"%self.params['cachepath_vol'])
            self._message("read in neighbors cache %s"%self.params['cachepath_nn'])


    def save_cache(self):
        """ """
        if self.params['cachepath_vol'] is None: return
        pickle.dump(self.vol_cache, file(self.params['cachepath_vol'],"w"))
        pickle.dump(self.neighbors_cache, file(self.params['cachepath_nn'],"w"))
        self._message("wrote out volume cache %s"%self.params['cachepath_vol'])
        self._message("wrote out neighbors cache %s"%self.params['cachepath_nn'])

    def plan(self):
        """Compute full tessellation.  It is not necessary to call this
        function because predict() will compute the parts of the
        tessellation it needs.

        Parameters
        ----------

        """
        tessparams = {'boundary': self.boundary,
                      'tess_npoints': self.params['tess_npoints'],
                      'tess_rad_frac': self.params['tess_rad_frac']}

        self._message("Computing cell volumes (N=%i)"%(len(self.sites)))
        t0 = time.time()
        tessellation._get_volume(self.tree, N.arange(len(self.sites)), tessparams, self.caches, return_neighbors=False, nproc=self.params['nproc'])
        self._message("Timing: fit %f sec"%(time.time()-t0))
        self.setup_done = True

    def predict(self, test_set=None, errors=None):
        """ Compute the redshift distribution.

        Parameters
        ----------
        test_set : array
        path : str
        """
        if test_set is not None:
            test_set = N.array(test_set)

        elif self.params['pcat'] is not None:
            test_set = N.array(ioutils.load_file(self.params['pcat']))

        else:
            return

        test_norm = self._normalize(test_set)

        if errors is not None:
            errors_norm = self._normalize(errors, errors=True)
        else:
            errors_norm = None

        t0 = time.time()

        z,pdf = self._skewer(test_norm, errors_norm)

        self._message("Timing: predict %f sec"%(time.time()-t0))

        return z, pdf


    def _get_skewered_cells(self, z, centers):
        """ """
        x = N.zeros((len(z), self.dim))
        x[:,0] = z

        cells = []

        for i,c in enumerate(centers):
            # construct array with redshift in first column and color
            x[:,1:] = c 

            # determine nearest neighbors along the skewer
            distance,matches = self.tree.query(x, k=1)
            cells.append(N.unique(matches))

        cells = N.concatenate(cells)

        return N.unique(cells)

    def _skewer(self, centers, errors=None):
        """ """  
        tessparams = {'boundary': self.boundary,
                      'tess_npoints': self.params['tess_npoints'],
                      'tess_rad_frac': self.params['tess_rad_frac']}

        zbounds = self.params['zrange']
        outbins = N.arange(zbounds[0],zbounds[1],self.params['zstep'])

        zz = N.arange(zbounds[0],zbounds[1],self.params['tess_zstep'])
        
        zzt = (zz-self.ztransform[0])/self.ztransform[1]

        error_offsets = None
        if errors is not None and self.params['error_samp_n'] > 0:
            self._message('Drawing samples within error region N=%i'%self.params['error_samp_n'])
            error_offsets = []
            cells = N.array([],dtype='int')
            for i, c in enumerate(centers):
                eps = N.random.normal(0, errors[i], (self.params['error_samp_n'], errors.shape[1]))
                error_offsets.append(eps)

        if self.setup_done is False:
            # start the clock
            t0 = time.time()

            # determine which cells are crossed
            whichcells = self._get_skewered_cells(zzt, centers)

            if error_offsets is not None:
                cells = N.array([],dtype='int')
                for i, c in enumerate(centers):
                    cells = N.concatenate([cells, self._get_skewered_cells(zzt, c + error_offsets[i])])
                cells = N.unique(cells)
                whichcells = N.concatenate([whichcells, cells])
                whichcells = N.unique(whichcells)
                error_offsets = N.array(error_offsets)

            self._message("Lookup volumes for %i cells"%len(whichcells))
            # compute volumes of these cells
            vol,neighbors = tessellation._get_volume(self.tree, whichcells, tessparams, self.caches, return_neighbors=True, nproc=self.params['nproc'])

            neighbors = N.concatenate(neighbors)
            neighbors = N.unique(neighbors)

            self._message("Lookup volumes for %i neighboring cells"%len(neighbors))

            # look up the volumes of neighboring cells
            vol_neighbors = tessellation._get_volume(self.tree, neighbors, tessparams, self.caches, return_neighbors=False, nproc=self.params['nproc'])

            self._message("Setup time: %f sec"%(time.time()-t0))

        args = []
        for i,c in enumerate(centers):
            x = N.zeros((len(zz), self.dim))
            x[:,0] = zzt
            x[:,1:] = c
            if error_offsets is not None:
                err = error_offsets[i]
            else:
                err = None
            args.append((self.tree, zz, x, err, outbins, tessparams, self.caches))

        self._message("Number of pdfs to compute: %i (nproc=%i)"%(len(args), self.params['nproc']))

        t0 = time.time()

        nloops,m = divmod(len(args),self.params['batch_size'])
        if m > 0:
            nloops += 1
        nloops = max(1, nloops)

        pdfs = []
        start = 0
        for loop in range(nloops):
            end = min(len(args), start + self.params['batch_size'])

            t1 = time.time()
            # Compute pdfs in multiple threads
            if self.params['nproc'] > 1:
                pool = Pool(self.params['nproc'])
                pdf_batch = pool.map(tessellation._compute_pdf, args[start:end])
                pool.close()
            else:
                pdf_batch = map(tessellation._compute_pdf, args[start:end])

            self._message("   pdf batch: %i/%i %i-%i ~ time: %3.2f sec"%(loop+1, nloops, start, end, time.time()-t1))

            pdfs.append(pdf_batch)
            start = end

        self._message("PDF time: %f sec"%(time.time()-t0))

        pdfs = N.vstack(pdfs)

        # redshift bin centers
        zcenter = (outbins[1:]+outbins[:-1])/2.

        # normalize pdf
        norm = N.sum(pdfs,axis=1)
        pdfs = pdfs.T/norm

        return zcenter, pdfs.T




# def test(dim=3,n=10000):
#     """ """
#     import pylab

#     data = N.random.normal([0.5,0,0],[0.1,0.2,0.2],(n,dim))
#     T = Tailz(data,tmpdir=None,verbose=True)

#     testdata = N.zeros([1,dim-1])
#     #T.fit()
#     z,pdf = T.predict(testdata)
    
#     m,v = utils.moments(z,pdf)

#     print "results"
#     print "-------"
#     print "mean",m
#     print "std dev",N.sqrt(v)

#     p = pdf[0]

#     pylab.plot(z,p)

#     model = N.exp(-(z-0.5)**2/2./.1**2)
#     model /= N.sum(model)
#     pylab.semilogy(z,model,dashes=(4,1))

#     ii = p>0
#     a = p[ii].min()
#     pylab.ylim(a,2*p.max())
#     pylab.xlim(z[ii].min(),z[ii].max())

#     pylab.show()

