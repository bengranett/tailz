import numpy as N


def load_file(path):
    """ """

    if path.endswith("npy" or "pickle"):
        stuff = N.load(path)

    else:
        stuff = N.loadtxt(path,unpack=True)

    return stuff



