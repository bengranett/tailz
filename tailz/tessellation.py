import sys,os
import numpy as N
import scipy
from scipy.interpolate import LinearNDInterpolator
import time

import geometry

from multiprocessing import Pool

def get_inside(tree,index,test):
    """Select points that are in a given Voronoi cell based upon a nearest-neighbor search.
    
    Parameters
    ----------
    tree : KDTree data structure
    index : int
    test : (n,dim) array

    Output
    ------
    ii : array of bool
    cells :

    """
    if len(test.shape)==1:
        test = N.array([test])
    d,ind = tree.query(test,k=1)

    ii = ind[:,0]==index
    cells = N.unique(ind)
    return ii,cells

def check_outside_boundary(boundary,test):
    """Determine if a point is exterior to a hypersphere.
    
    Parameters
    ----------
    boundary : (center, radius^2) tuple
        Boundary hypersphere that encloses the tessellation with center and radius.
    test : (n,d) array
        Test points

    Output
    ------
    bool array
    
    """
    r2 = boundary[1]
    p = test - boundary[0]
    return N.sum(p**2,axis=1) < r2


def sample_cell_rad(tree,  
                    index,
                    boundary,
                    rad_frac=10.,
                    npoints=100,
                    verbose=False, 
                    max_loops=1000000, 
                    return_samples=False):
    """ Monte Carlo sample a voronoi cell to compute the volume and neighbors.

    Parameters
    ----------
    tree : KDTree structure
        Tree data structure representing the sites of the Voronoi cells
    index : int
        Index of cell to be measured
    boundary : (center, radius) tuple
        Boundary hypersphere that encloses the tessellation with center and radius.
    rad_frac : float
        Step size in radius is 0.5*R_NN/rad_frac where R_NN is the distance to nearest neighbor
    npoints : int
        Number of points to sample the hypersphere
    max_loops : int
        If the algorithm is still going after this many loops, stop.
    return_samples : bool
        If True, collect the array of samples and return it.
    verbose : bool
        If True, print info to the screen.

    Outputs
    -------
    volume : float
        Computed cell volume
    samples : array
        If return_samples, array of samples, otherwise empty array
    hitboundary : bool
        Flag indicating if the boundary was hit, in this case volume is 0.
    neighbors : array
        List of indices of the Delaunay neighbors.
    """
    t0 = time.time()
    cent = tree.data[index]

    dim = len(cent)

    # centroid of the point distribution
    center = N.mean(tree.data,axis=0)

    # determine nearest neighbors to set search region and density of test points
    d,ind = tree.query([cent], k=2)
    rmin = N.max(d)/2.                # half distance to nearest neighbor
    step = rmin*1./rad_frac           # step

    vol_0 = geometry.sphere_volume(rmin,dim)

    samples = []
    weights = []

    volume = vol_0

    radius = rmin

    test = geometry.sample_sphere(dim,radius,step,npoints) + cent

    dvol0 = geometry.sphere_surface_area(rmin,dim)*step*1./npoints

    neighbors = []

    hitboundary = False
    loop = 0
    count = 0
    while not hitboundary:
        loop += 1
        if loop>max_loops:
           print "hit",loop
           print "nsamp",len(samples),count
           break

        # increment radius
        test = (test - cent)*(radius+step)/radius + cent
        radius += step

        # Also scale the density as the sphere expands
        dvol = dvol0*(radius/rmin)**(dim-1)

        # remove test points beyond the boundary
        closer = check_outside_boundary(boundary,test)
        if not N.all(closer):
            hitboundary=True
        
        ntest = N.sum(closer)
        if ntest == 0:
            # if test points are finished, end iterations
            break

        test = test[closer]

        # determine cells inside the cell
        inside,whichcells = get_inside(tree,index,test)

        neighbors.append(whichcells)

        ntest = N.sum(inside)
        if ntest==0: 
            # if test points are finished, end iterations
            break

        count += ntest
        test = test[inside]
        if return_samples:
            samples.append(test)
            weights.append(N.ones(ntest)*dvol)

        # sum up the volume
        volume += ntest*dvol

    # If the cell crosses the exterior tessellation boundary, it is
    # unbounded so the volume is undefined.  In this case set to 0.
    if hitboundary:
        volume = 0

    if return_samples:
        samples = N.vstack(samples)
        weights = N.concatenate(weights)

    if loop>1:
        neighbors = N.concatenate(neighbors)
    neighbors = N.unique(neighbors)
    neighbors.sort()
    samples = N.array(samples)

    if verbose: print "cell %i v=%f time=%g sec"%(index,volume,time.time()-t0)

    return volume,samples,hitboundary,neighbors


def _sample_cell_rad(params):
    """ Wrapper to unpack the parameters tuple. """
    return sample_cell_rad(*params)



def _get_volume(tree, cells, tessparams, caches, return_neighbors=True, nproc=1, verbose=False):
    """ compute volume of cell i

    Parameters
    ----------


    Output
    ------
    """
    # unpack inputs
    boundary = tessparams['boundary']
    rad_frac = tessparams['tess_rad_frac']
    npoints = tessparams['tess_npoints']
    vol_cache,neighbors_cache = caches

    uniquecells = N.unique(cells)

    args = []
    lookup = []
    nhit = 0
    for celli in uniquecells:
        if not vol_cache.has_key(celli):
            lookup.append(celli)
            args.append((tree, celli, boundary, rad_frac, npoints))
        else:
            nhit += 1

    if verbose: print "cache hits: %i, new: %i"%(nhit, len(lookup))

    # Compute these cells in multiple threads
    if nproc > 1:
        pool = Pool(nproc)
        results = pool.map(_sample_cell_rad, args)
        pool.close()
    else:
        results = map(_sample_cell_rad, args)

    for i in range(len(results)):
        vol,samp,hitboundary,nn = results[i]
        j = lookup[i]
        vol_cache[j] = vol
        neighbors_cache[j] = nn

    volume = []
    neighbors = []

    for celli in cells:
        volume.append(vol_cache[celli])
        if return_neighbors:
            neighbors.append(neighbors_cache[celli])

    volume = N.array(volume)

    if return_neighbors:
        return volume,neighbors
    else:
        return volume

def _compute_pdf(args):
    """ Compute PDF

    Parameters
    ----------

    Output
    ------

    """
    tree, zz, x, error_offsets, outbins, tessparams, caches = args

    if error_offsets is None:
        error_offsets = N.array([N.zeros(x.shape[1]-1)])
    else:
        error_offsets = N.vstack([N.zeros(x.shape[1]-1), error_offsets])

    bigx = []
    for i in range(len(error_offsets)):
        xoff = x.copy()
        xoff[:,1:] += error_offsets[i]
        bigx.append(xoff)

    # determine nearest neighbors along the skewer
    distance,matches = tree.query(N.vstack(bigx), k=1)
    matches = matches[:,0]

    # begin with intersecting cells
    vol,neighbors = _get_volume(tree, matches, tessparams, caches, return_neighbors=True, nproc=1)

    neighbors = N.concatenate(neighbors)
    neighbors = N.concatenate([neighbors,N.unique(matches)])
    neighbors = N.unique(neighbors)

    # look up the volumes of neighboring cells
    vol_neighbors = _get_volume(tree, neighbors, tessparams, caches, return_neighbors=False, nproc=1)

    vol_neighbors = N.array(vol_neighbors)
    rho = N.zeros(len(vol_neighbors))
    ii = vol_neighbors>0
    rho[ii] = 1./vol_neighbors[ii]

    assert(N.all(N.isfinite(rho)))

    dens_int = N.zeros(len(outbins)-1,dtype='d')

    # construct a linear interpolator based upon delaunay triangulation
    try:
        interp = LinearNDInterpolator(N.array(tree.data)[neighbors], rho, fill_value=0)
    except scipy.spatial.qhull.QhullError:
        return dens_int

    # compute interpolated density along the skewer
    count,e = N.histogram(zz, bins=outbins)
    nonzero = count>0

    for v in bigx:
        try:
            mass_int = interp(v)
        except scipy.spatial.qhull.QhullError:
            continue

        h,e = N.histogram(zz, bins=outbins, weights=mass_int)
        dens_int[nonzero] += h[nonzero]/count[nonzero]

    return dens_int


